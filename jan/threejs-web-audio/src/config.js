const dat = require('dat.gui')

export class Config {
    constructor() {
        this.ballBass = 5
        this.ballTreble = 5
        this.ballDeformAmp = 3
        this.rotation = 0.005
        this.planetBaseline = 30
        this.planetBass = 5
        this.planetTreble = 5
        this.planetDeformAmp = 50
        this.sunBass = 15

        const gui = new dat.GUI()
        gui.add(this, 'rotation', -0.02, 0.02)
        gui.add(this, 'ballBass', 0, 20)
        gui.add(this, 'ballTreble', 0, 20)
        gui.add(this, 'ballDeformAmp', 0, 10)
        gui.add(this, 'planetBaseline', 0, 100)
        gui.add(this, 'planetBass', 0, 20)
        gui.add(this, 'planetTreble', 0, 20)
        gui.add(this, 'planetDeformAmp', 0, 50)
        gui.add(this, 'sunBass', 0, 100)
    }
}

export default {
    Config,
}
