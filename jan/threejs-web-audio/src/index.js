import { Visualizer } from './visualizer'

const init = () => {
    const file = document.getElementById('file-input')
    const fileLabel = document.querySelector('label.file')
    const audio = document.getElementById('audio')
    const visualizer = new Visualizer(audio)

    file.onchange = () => {
        fileLabel.classList.add('normal')
        audio.classList.add('active')

        const { files } = file

        audio.src = URL.createObjectURL(files[0])
        audio.load()
        audio.play()
        visualizer.play()
    }
}

window.onload = init
