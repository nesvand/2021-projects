export const normalize = (val, minVal, maxVal) =>
    (val - minVal) / (maxVal - minVal)
export const scale = (val, minVal, maxVal, outMin, outMax) =>
    normalize(val, minVal, maxVal) * (outMax - outMin) + outMin
export const avg = (arr) => arr.reduce((sum, b) => sum + b) / arr.length
export const max = (arr) =>
    arr.reduce((a, b) => Math.max(a, b), Number.MIN_VALUE)
export const min = (arr) =>
    arr.reduce((a, b) => Math.min(a, b), Number.MAX_VALUE)

export default {
    normalize,
    scale,
    avg,
    max,
    min,
}
