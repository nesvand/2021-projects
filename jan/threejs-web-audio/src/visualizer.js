import * as THREE from 'three'
import { avg, normalize, scale } from './utils'
import { Config } from './config'

const data = new Config()
const SimplexNoise = require('simplex-noise')
const noise = new SimplexNoise()

export class Visualizer {
    constructor(audioElement) {
        this.audio = audioElement
    }

    deformSun(mesh, bassIntensity) {
        const offset = mesh.geometry.parameters.radius

        mesh.geometry.vertices.forEach((vertex) => {
            vertex.normalize()
            vertex.multiplyScalar(offset + bassIntensity)
        })

        mesh.geometry.verticesNeedUpdate = true
        mesh.geometry.normalsNeedUpdate = true
        mesh.geometry.computeVertexNormals()
        mesh.geometry.computeFaceNormals()
    }

    deformPlanet(mesh, bassIntensity, trebleIntensity) {
        const offset = mesh.geometry.parameters.radius
        const t = Date.now() * 0.00001

        mesh.geometry.vertices.forEach((vertex) => {
            vertex.normalize()

            // Local coords for X will range from -1 to 1 (left to right),
            // so we'll carve out ~40% of the middle and keep it flat to
            // simulate the canyon
            let dist = 0
            if (vertex.x < -0.2 || vertex.x > 0.2) {
                // Bass will scale the planet evenly, treble will scale
                // the noise deformation
                dist =
                    data.planetBaseline +
                    bassIntensity +
                    scale(
                        noise.noise3D(
                            vertex.x + t * 19,
                            vertex.y + t * 23,
                            vertex.z + t * 29,
                        ),
                        -1,
                        1,
                        -0.5,
                        1,
                    ) *
                        data.planetDeformAmp *
                        trebleIntensity
            }

            vertex.multiplyScalar(offset + dist)
        })

        mesh.geometry.verticesNeedUpdate = true
        mesh.geometry.normalsNeedUpdate = true
        mesh.geometry.computeVertexNormals()
        mesh.geometry.computeFaceNormals()
    }

    deformBall(mesh, bassFr, treFr) {
        const offset = mesh.geometry.parameters.radius
        const t = Date.now() * 0.0001

        mesh.geometry.vertices.forEach((vertex) => {
            vertex.normalize()

            // Bass will scale the planet evenly, treble will scale
            // the noise deformation
            vertex.multiplyScalar(
                offset +
                    bassFr +
                    scale(
                        noise.noise3D(
                            vertex.x + t * 2,
                            vertex.y + t * 5,
                            vertex.z + t * 11,
                        ),
                        -1,
                        1,
                        -0.5,
                        1,
                    ) *
                        data.ballDeformAmp *
                        treFr,
            )
        })

        mesh.geometry.verticesNeedUpdate = true
        mesh.geometry.normalsNeedUpdate = true
        mesh.geometry.computeVertexNormals()
        mesh.geometry.computeFaceNormals()
    }

    render(dataArray) {
        this.analyser.getByteFrequencyData(dataArray)

        const bassFrequencies = dataArray.slice(0, dataArray.length / 2 - 1)
        const trebleFrequences = dataArray.slice(
            dataArray.length / 2 - 1,
            dataArray.length - 1,
        )

        const bassAvg = avg(bassFrequencies) / bassFrequencies.length
        const trebleAvg = avg(trebleFrequences) / trebleFrequences.length

        this.deformSun(this.sun, scale(bassAvg, 0, 1, 0, data.sunBass))
        this.deformBall(
            this.ball,
            scale(bassAvg, 0, 1, 0, data.ballBass),
            scale(trebleAvg, 0, 1, 0, data.ballTreble),
        )
        this.deformPlanet(
            this.planet,
            scale(bassAvg, 0, 1, 0, data.planetBass),
            scale(trebleAvg, 0, 1, 0, data.planetTreble),
        )

        this.ball.rotation.y += data.rotation * 0.66
        this.planet.rotation.x += data.rotation * 1.2
        this.renderer.render(this.scene, this.camera)
        requestAnimationFrame(this.render.bind(this, dataArray))
    }

    onWindowResize() {
        this.camera.aspect = window.innerWidth / window.innerHeight
        this.camera.updateProjectionMatrix()
        this.renderer.setSize(window.innerWidth, window.innerHeight)
    }

    play() {
        this.context = new AudioContext()
        const src = this.context.createMediaElementSource(this.audio)
        this.analyser = this.context.createAnalyser()

        src.connect(this.analyser)
        this.analyser.connect(this.context.destination)
        this.analyser.fftSize = 512

        this.scene = new THREE.Scene()
        this.camera = new THREE.PerspectiveCamera(
            45,
            window.innerWidth / window.innerHeight,
            0.1,
            1000,
        )

        this.camera.position.set(0, 20, 100)
        this.scene.add(this.camera)

        this.renderer = new THREE.WebGLRenderer({
            alpha: true,
            antialias: true,
        })

        this.renderer.setSize(window.innerWidth, window.innerHeight)

        const sunGeometry = new THREE.IcosahedronGeometry(133, 6)
        const sunMaterial = new THREE.MeshLambertMaterial({
            color: 0x502000,
            emissive: 0xcc8800,
            transparent: true,
            opacity: 0.9,
        })
        const sunWireframeMaterial = new THREE.MeshBasicMaterial({
            color: 0xeeeeaa,
            wireframe: true,
            wireframeLinewidth: 2,
            opacity: 0.1,
        })
        this.sun = new THREE.Mesh(sunGeometry, sunMaterial)
        const sunWireframe = new THREE.Mesh(sunGeometry, sunWireframeMaterial)

        this.sun.add(sunWireframe)
        this.sun.position.set(0, -200, -500)
        this.scene.add(this.sun)

        const planetGeometry = new THREE.IcosahedronGeometry(400, 12)
        const planetMaterial = new THREE.MeshBasicMaterial({
            color: 0x000522,
        })
        const planetWireframeMaterial = new THREE.MeshLambertMaterial({
            color: 0x00aaff,
            wireframe: true,
            wireframeLinewidth: 4,
            opacity: 0.4,
        })
        this.planet = new THREE.Mesh(planetGeometry, planetMaterial)
        const planetWireframe = new THREE.Mesh(
            planetGeometry,
            planetWireframeMaterial,
        )

        this.planet.add(planetWireframe)
        this.planet.position.set(0, -450, 0)
        this.scene.add(this.planet)

        const ballGeometry = new THREE.IcosahedronGeometry(5, 6)
        const ballMaterial = new THREE.MeshBasicMaterial({
            color: 0xff00ee,
            transparent: true,
            opacity: 0.2,
        })
        const ballWireframeMaterial = new THREE.MeshLambertMaterial({
            color: 0xff00ee,
            wireframe: true,
            wireframeLinewidth: 4,
        })
        this.ball = new THREE.Mesh(ballGeometry, ballMaterial)
        const ballWireframe = new THREE.Mesh(
            ballGeometry,
            ballWireframeMaterial,
        )

        this.ball.add(ballWireframe)
        this.ball.position.set(0, 0, 30)
        this.scene.add(this.ball)
        this.camera.lookAt(this.ball.position)

        const ambientLight = new THREE.AmbientLight(0xaaaaaa)

        this.scene.add(ambientLight)

        const spotLight = new THREE.SpotLight(0xffffff)

        spotLight.intensity = 0.5
        spotLight.position.set(-10, 40, 50)
        spotLight.lookAt(this.ball.position)
        spotLight.castShadow = true
        this.scene.add(spotLight)

        this.render(new Uint8Array(this.analyser.frequencyBinCount))

        document.getElementById('out').appendChild(this.renderer.domElement)
        window.addEventListener('resize', this.onWindowResize.bind(this), false)
    }
}

export default {
    Visualizer,
}
